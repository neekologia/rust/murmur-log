use futures::{
	channel::mpsc::{channel, Receiver},
	SinkExt, StreamExt,
};
use notify::{Config, Event, RecommendedWatcher, RecursiveMode, Watcher};
use regex::Regex;
use std::{
	fs::File,
	io::{Read, Write},
	path::Path, os::unix::net::UnixStream,
};
use tokio::time::{sleep, Duration};

const MUR_SOCK: &str = "/tmp/mur-rs.sock";

fn async_watcher() -> notify::Result<(RecommendedWatcher, Receiver<notify::Result<Event>>)> {
	let (mut tx, rx) = channel(1);

	let watcher = RecommendedWatcher::new(
		move |res| {
			futures::executor::block_on(async {
				tx.send(res).await.unwrap();
			})
		},
		Config::default(),
	)?;

	Ok((watcher, rx))
}

async fn async_watch<P: AsRef<Path>>(path: P) {
	let (mut watcher, mut rx) = async_watcher().unwrap();
	watcher.watch(path.as_ref(), RecursiveMode::Recursive).unwrap();

	while let Some(res) = rx.next().await {
		match res {
			Ok(event) => {
				event_handler(event).await;
			}
			Err(e) => println!("watch error: {:?}", e),
		}
	}
}

async fn event_handler(ev: Event) {
	match ev.kind {
		notify::EventKind::Modify(_) => {
			let mut buf = String::new();
			File::open("murmur.log")
				.unwrap()
				.read_to_string(&mut buf)
				.unwrap();
			let mut last = buf.lines().last().unwrap().to_string();
			if !last.contains("Authenticated") && !last.contains("Connection closed") {
				return;
			}
			if last.contains("<0:(-1)>") {
				return;
			}
			let Some(split) = last.split_once("=>") else {
				return;
			};
			last = split.1.trim().to_string();
			let r = Regex::new(r"<[0-9]*").unwrap();
			last = r.replace(&last, "").to_string();
			let r = Regex::new(r"\(-[0-9]\):|\([0-9]\):").unwrap();
			last = r.replace(&last, "").to_string();
			let r = Regex::new(r":").unwrap();
			last = r.replace(&last, "").to_string();
			let r = Regex::new(r"\([0-9]\)>|\(-[0-9]\)>").unwrap();
			last = r.replace(&last, "").to_string();
			let r = Regex::new(r":.*").unwrap();
			last = r.replace(&last, "").to_string();
			last = format!("(murmur) {last}");

			let mut stream = loop {
				match UnixStream::connect(MUR_SOCK) {
					Ok(stream) => {
						break stream;
					}
					Err(_) => {
						sleep(Duration::new(10, 0)).await;
						continue;
					}
				}
			};
			stream.write_all(last.as_bytes()).unwrap();
			stream.shutdown(std::net::Shutdown::Both).unwrap();
		}
		_ => (),
	}
}

#[tokio::main]
async fn main() {
	let path = "murmur.log";
	async_watch(path).await;
}
